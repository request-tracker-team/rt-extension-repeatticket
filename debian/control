Source: rt-extension-repeatticket
Section: perl
Priority: optional
Maintainer: Debian Request Tracker Group <pkg-request-tracker-maintainers@lists.alioth.debian.org>
Uploaders: Joost van Baal-Ilić <joostvb@debian.org>,
           Maarten Horden <m.horden+debian@parsus.nl>,
           Dominic Hargreaves <dom@earth.li>,
           Andrew Ruthven <andrew@etc.gen.nz>,
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libdatetime-event-ical-perl,
 libmodule-install-rtx-perl (>= 0.42-1~),
 libmodule-install-substitute-perl,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/request-tracker-team/rt-extension-repeatticket
Vcs-Git: https://salsa.debian.org/request-tracker-team/rt-extension-repeatticket.git
Homepage: https://metacpan.org/release/RT-Extension-RepeatTicket
Testsuite: autopkgtest-pkg-perl

Package: rt5-extension-repeatticket
Architecture: all
Breaks: rt4-extension-repeatticket (<< 1.11-2)
Depends: ${misc:Depends}, ${perl:Depends},
 libdatetime-event-ical-perl,
 request-tracker5,
Description: Repeat tickets in Request Tracker based on a schedule (for RT5)
 The RepeatTicket extension for the Request Tracker trouble-ticket tracking
 system allows you to set up recurring tickets so new tickets are
 automatically created based on a schedule. The new tickets are populated
 with the subject and initial content of the original ticket in the
 recurrence.
 .
 After you activate the plugin all tickets will have a Recurrence tab on the
 create and edit pages. To set up a repeating ticket, click the checkbox to
 "Enable Recurrence" and fill out the schedule for the new tickets.
 .
 New tickets are created when you initially save the recurrence, if new
 tickets are needed, and when your daily cron job runs the rt-repeat-ticket
 script.
