rt-extension-repeatticket (2.03-1) unstable; urgency=medium

  * Remove deprecated fields from d/upstream/metadata.
  * Enable basic autopkgtests.
  * Deploy cron file to run rt-repeat-ticket.
  * Ensure we can build source after successful build (Closes: #1047311).
  * Move manpages to be alternatives to support co-installing with
    rt4-extension-repeatticket.
  * Switch to debhelp-compat method.
  * Bump d/watch to version 4.
  * Don't need root to run tests.
  * Stop cron.daily from emitting emails if package is removed.
  * New upstream release.
  * Bump d/copyright. 
  * New Standards-Version: 4.7.0 (no changes).
  * Generate HTML docs and register with doc-base. 
  * Enable autopkgtest-pkg-perl testsuite.
  * Upload to unstable (Closes: #983634).

 -- Andrew Ruthven <andrew@etc.gen.nz>  Sun, 11 Aug 2024 16:36:54 +1200

rt-extension-repeatticket (2.00-1) experimental; urgency=medium

  * New upstream release, only for RT5 (Closes: #983634).
  * New Standards-Version: 4.6.2

 -- Andrew Ruthven <andrew@etc.gen.nz>  Mon, 06 Mar 2023 00:20:02 +1300

rt-extension-repeatticket (1.11-1) unstable; urgency=medium

  [ Maarten Horden ]
  * debian/changelog: fixed spelling error.
  * debian/control: correct URL for Vcs-Browser.

  [ Dominic Hargreaves ]
  * Update Vcs-* fields to point to Salsa
  * Update README.Debian with current information about configuring
    plugins (Closes: #898605)

  [ Andrew Ruthven ]
  * New upstream release
  * Use debhelper compat 13
  * New Standards-Version: 4.5.1

  [ Dominic Hargreaves ]
  * Build RT4 package targeted at bullseye
  * Add myself and Andrew to Uploaders

 -- Dominic Hargreaves <dom@earth.li>  Sun, 07 Feb 2021 12:09:46 +0000

rt-extension-repeatticket (1.10-5) unstable; urgency=medium

  [ Maarten Horden ]
  * debian/README.Debian: changed: use rt-setup-database for inserting data.

  [ Joost van Baal-Ilić ]
  * debian/control: remove libmodule-install-rtx-perl from Depends, reflecting
    updated post installation instructions in README.Debian.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 26 Sep 2016 07:07:52 +0200

rt-extension-repeatticket (1.10-4) unstable; urgency=medium

  * debian/rules, debian/manpages: build and install rt-repeat-ticket(1)
    manpage.
  * debian/control: add libmodule-install-rtx-perl to Depends, since that's
    needed during post installation as listed in README.Debian; thanks
    Maarten Horden for reporting.
  * debian/install: do install repeat-ticket.css, thanks Maarten Horden for
    reporting.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Thu, 22 Sep 2016 13:57:18 +0200

rt-extension-repeatticket (1.10-3) unstable; urgency=medium

  [ Maarten Horden ]
  * debian/control: added dependency for request-tracker4 (>= 4.0.6)
  * debian/control: shortened package short description

  [ Joost van Baal-Ilić ]
  * Initial release. (Upload 1.10-2 got REJECT-ed, on our request.)
    Closes: #834969
  * Moved from Alioth repo pkg-perl/librt-extension-repeatticket-perl.git to
    pkg-request-tracker/rt-extension-repeatticket.git, as suggested by
    Dominic Hargreaves (thanks!).  (RT::Extension::RepeatTicket is more like
    rt-extension-nagios and other pkg-request-tracker maintained packages than
    like the pkg-perl CPAN modules.)
    - debian/control: change Maintainer from Debian Perl Group to Debian Request
      Tracker Group <pkg-request-tracker-maintainers/a/lists.alioth.d.o>
    - debian/control: update Vcs-Browser and Vcs-Git
    - Commits should (?) now get sent to
      pkg-request-tracker-commits-request@lists.alioth.d.o
  * Renamed from librt-extension-repeatticket-perl to rt-extension-repeatticket
    (source) and rt4--extension-repeatticket (binary)
    - debian/control: update Source and Package.
  * debian/control: Add Maarten Horden to Uploaders; Maarten is mhorden-guest @
    alioth.
  * debian/docs: install upstream README.
  * Do not use upstream Makefile's "install" target: it lacks the needed DESTDIR
    support.
    - debian/rules: add override_dh_auto_install-indep
    - debian/install: added: rt-repeat-ticket gets installed under /u/bin, rest
      under
      u/s/request-tracker4/plugins/RT-Extension-RepeatTicket/{etc,html,lib,po} .
    - debian/manpages: install RT::Extension::RepeatTicket.3pm.
  * debian/patches/remove_inc.patch: added, but currently _not_ used (!): This
    patch would prevent using the upstream supplied Module::Install::RTx, and
    would have us use the one shipped with Debian.
  * debian/TODO: added.
  * debian/README.Debian: added: post installation instructions.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Tue, 20 Sep 2016 20:35:21 +0200

librt-extension-repeatticket-perl (1.10-2) unstable; urgency=low

  [ Joost van Baal-Ilić ]
  * Initial release. Closes: #834969
  * No longer ship an empty binary package: use tricks as used in
    rt4-extension-nagios (thanks Dominic Hargreaves)
    - debian/control: add libmodule-install-rtx-perl to Build-Depends-Indep
    - debian/rules: set RTHOME

  [ Maarten Horden ]
  * debian/{copyright,control}: completed.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 21 Aug 2016 10:51:36 +0200

librt-extension-repeatticket-perl (1.10-1) unstable; urgency=low

  * Initial dh-make-perl generated package, unreleased.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 21 Aug 2016 07:55:15 +0200
